import java.lang { StringBuffer, IllegalStateException }
import ceylon.collection { LinkedList }
import ceylon.io.buffer { CharacterBuffer }

shared class SExpression({SExpression|String*} values = {}) extends LinkedList<SExpression|String>(values){
}

abstract class ParseState() of inAtom | inWhitespace | inQuotedAtom {}
object inAtom extends ParseState(){}
object inWhitespace extends ParseState(){}
object inQuotedAtom extends ParseState(){}

shared SExpression parseExpressions(CharacterBuffer reader) {
	value list = SExpression();
	variable Character c;
	while ((c = reader.get()) != -1) {
		if (c == '(') {
			list.add(parseList(reader));
		}
	}
	return list;
}

shared SExpression? parseExpression(CharacterBuffer reader) {
	variable Character c;
	while ((c = reader.get()) != -1) {
		if (c == '(') {
			return parseList(reader);
		}
	}
	return null;
}

SExpression parseList(CharacterBuffer reader) {
	value root = SExpression();
	variable ParseState state = inWhitespace;
	value atom = StringBuffer();
	variable Character c;
	while ((c = reader.get()) != -1) {
		switch (state) 
		case (inWhitespace) {
			if (!Character.whitespace(c)) {
				if (c == '(') {
					root.add(parseList(reader));
				} else if (c == ')') {
					return root;
				} else if (c == '"') {
					atom.setLength(0);
					state = inQuotedAtom;
				} else {
					atom.setLength(0);
					atom.append(c);
					state = inAtom;
				}
			}
		}
		case (inAtom) {
			if (Character.whitespace(c)) {
				root.add(atom.string);
				state = inWhitespace;
			} else if (c == ')') {
				root.add(atom.string);
				return root;
			} else {
				atom.append(c);
			}
		}
		case (inQuotedAtom) {
			if (c == '"') {
				root.add(atom.string);
				state = inWhitespace;
			} else {
				atom.append(c);
			}
		}
	}
	throw IllegalStateException();
}

void test(){
	SExpression? expr = parseExpression(CharacterBuffer("(a b c (d e f))"));
	if(exists expr){
		print(expr);
		print(expr[0]?.string);
		if(exists thirdAtom=expr[2]){
			print(thirdAtom);
		}
	}
}

